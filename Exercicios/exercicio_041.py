# Faça um programa que leia a idade de um nadador e classifique em:
# Até 9 anos: MIRIM
# Até 14 anos: INFANTIL
# Até 19 anos: JUNIOR
# Até 20 anos: SÊNIOR
# Acima : Master

year = int(input('swimmer is age '))

if year <= 9: 
    print('Little Swimmer')
elif year > 9 and year <= 14:
    print('Childish Swimmer') 
elif year > 14 and year <= 19:
    print('Junior Swimmer')
elif year == 20:
    print('Senior Swimmer')
elif year > 20:
    print('Master Swimmer')