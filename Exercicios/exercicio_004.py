# Receba algo do teclado e exibas as informações possiveis
infor = input("Entrada ")

print(" A entrada {} é um número? ".format(infor))
if infor.isnumeric() == True:
    print("Sim")
else: 
    print("Não")

print(" A entrada {} é um uma letra? ".format(infor))
if infor.isalpha() == True:
    print("Sim")
else: 
    print("Não")

print(" A entrada {} é um decimal? ".format(infor))
if infor.isdecimal() == True:
    print("Sim")
else: 
    print("Não")

print("Qual o Tipo de {}".format(infor))
print(type(infor))