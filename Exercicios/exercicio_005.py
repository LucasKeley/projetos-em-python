# Faça um programa que leira um número inteiro e mostre seu sucessor e seu antecessor
import math

num = int(input("Digite um Número "))
atnum = num - 1
sunum = num + 1

print(' O antecessor de {} é {}'.format(num, atnum))
print(' O sucessor de {} é {}'.format(num, sunum))