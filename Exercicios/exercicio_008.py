# Escreve um programa que leia um valor em metros e retorne seu valor
# em centimetros e milimetros

metros = float(input('Quantos metros precisa converter? '))
centimetros = metros * 100
milimetros = centimetros * 100

print('{} são {} cm equivalente a {} mm '.format(metros, centimetros, milimetros))

