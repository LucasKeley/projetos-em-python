# Diga se um numero inteiro recebio pelo teclado é:
#  par ou impar

number = int(input('What is number? '))
if (number % 2) == 0:
    print('The number {} is pair'.format(number))
else:
    print('The number {} is odd'.format(number))