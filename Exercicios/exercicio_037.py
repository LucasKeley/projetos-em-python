number = int(input('\033[1;32mNUMBER CONVERSION\033[m'))

print(''' Choose an option
    [1] - BINARY CONVERSION
    [2] - HEX CONVERSION
    [3] - OCTAL CONVERSION
 ''')
option = int(input('Whant is option? '))
if option == 1 :
    print(bin(number)[2:])
elif option == 2:
    print(hex(number)[2:])
elif option == 3:
    print(oct(number)[2:])
else:
    print('\033[1;31mINVALID OPTION\033[m')
