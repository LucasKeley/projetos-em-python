# Mostre se uma pessoa está no tempo de alistamento, passou o tempo ou ainda precisara se alistar

import datetime

year_today = datetime.date.today().year
year_birth = int(input('\033[1;32mWhat is year of birth?\033[m '))
age = year_today - year_birth

if age < 18:
    print('\033[1;33m{} Years left for military enlistment\033[m'.format(18-age))
elif age == 18:
    print('\033[1;32mthis time to make the military enlistment\033[m')
elif age > 18:
    print('\033[1;31mit is past time to make the military enlistment, should have enlisted in {} \033[m'.format(year_birth+18))
