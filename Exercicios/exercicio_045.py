# Faça um programa que jogue pedra, papel e tesoura com o usuario.
import random

jolkenpo = ['Pedra', 'Papel', 'Tesoura']
oponente = (random.choice(jolkenpo))

print(''' 
    Escolha uma opção:
    [1] - Pedra
    [2] - Papel
    [3] - Tesoura
     ''')

opcao = int(input(''))

if opcao == 1:
    usuario = 'Pedra'
    print('O oponente escolheu {} e o Usuario escolheu {}'.format(oponente, usuario))
    if oponente == usuario:
        print('EMPATE')
    elif oponente == 'Tesoura':
        print('VOCE VENCEU')
    elif oponente == 'Papel'  :
        print('VOCE PERDEU')
elif opcao == 2:
    usuario = 'Papel'
    print('O oponente escolheu {} e o Usuario escolheu {}'.format(oponente, usuario))
    if oponente == usuario:
        print('EMPATE')
    elif oponente == 'Tesoura':
        print('VOCE PERDEU')
    elif oponente == 'Pedra'  :
        print('VOCE VENCEU')
elif opcao == 3:
    usuario = 'Tesoura'
    print('O oponente escolheu {} e o Usuario escolheu {}'.format(oponente, usuario))
    if oponente == usuario:
        print('EMPATE')
    elif oponente == 'Pedra':
        print('VOCE PERDEU')
    elif oponente == 'Papel'  :
        print('VOCE VENCEU')
else: 
    print('Sem opção')

