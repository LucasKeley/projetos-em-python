# Calcule o preço de uma viagem 
# R$ 0,50 para cada Km de uma viagem até 200 km
# R$ 0,45 pra cada Km maiores de 200 Km

km = float(input('How many Km is the trip? '))

if km <= 200: 
    print('you will pay R$ {:.2f}'.format(km*0.50))
else:
    print('You will pay R$ {:.2F}'.format(km*0.45))
