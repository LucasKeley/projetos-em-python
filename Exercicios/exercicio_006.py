# Crie um algoritimo que mostre o doblo, triplo e a raiz de úm número

from math import sqrt

num = float(input('Entre com um Número '))

dobro = num * 2
triplo = num * 3
raiz = sqrt(num)

print('{} x 2 = {}'.format(num, dobro))
print('{} x 3 = {}'.format(num, triplo))
print('raiz({}) = {}'.format(num, raiz))