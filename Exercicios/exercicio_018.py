# ler um angulo e mostra o seno, coseno e tangente
import math
angle = float(input('Put the angle ')) # recebe em angulo
rad = math.radians(angle) # converte para radeando
print('the sin in {} is {:.2f}'.format(angle, math.sin(rad)))
print('the cos in {} is {:.2f}'.format(angle, math.cos(rad)))
print(' the tang is {} is {:.2f}'.format(angle, math.tan(rad)))
