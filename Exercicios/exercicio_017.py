# Calcula a hipotenusa de um triangulo retangulo

from math import hypot
 
a = float(input('Put the opposite in cm '))
b = float(input('Put the adjacent in cm '))
print('The hypotenuse is {:.2f} cm'.format(hypot(a, b)))