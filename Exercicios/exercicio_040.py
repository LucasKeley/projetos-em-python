# Calcule o media de uma aluno recebendo a nota de 2 provas:
# Provado 7,0 á 10
# Recuperação 6,9 a 5
# Reprovado baixo de 5

grade_1 = float(input('Put with test grade [1] '))
grade_2 = float(input('Put with test grade [2] '))
average = (grade_1 + grade_2) / 2

if average > 7 and average <= 10:
    print('\033[1;32mApproved\033[m')
elif average < 7 and average >= 5:
    print('\033[1;33mRecovery\033[m')
elif average < 5:
    print('\033[1;31mDisapproved\033[m')