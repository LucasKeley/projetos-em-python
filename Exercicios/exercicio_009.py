# faça um programa que mostre a tabuada de um número
# entre 0 e 10
# não use laços de repetição

value = int(input('Want to see the multiplication table of what vaule? ')) 

print(' {} x {} = {}'.format(value, 0, value*0))
print(' {} x {} = {}'.format(value, 1, value*1))
print(' {} x {} = {}'.format(value, 2, value*2))
print(' {} x {} = {}'.format(value, 3, value*3))
print(' {} x {} = {}'.format(value, 4, value*4))
print(' {} x {} = {}'.format(value, 5, value*5))
print(' {} x {} = {}'.format(value, 6, value*6))
print(' {} x {} = {}'.format(value, 7, value*7))
print(' {} x {} = {}'.format(value, 8, value*8))
print(' {} x {} = {}'.format(value, 9, value*9)) 
print(' {} x {} = {}'.format(value, 10, value*10))