# Leia 3 números e diga qual o maior e qual o menor
import math

num1 = int(input('What is number? '))
num2 = int(input('What is number? '))
num3 = int(input('What is number? '))

print(max(num1, num2, num3)) # Maneira mais pratica de saber o maior
print(min(num1, num2, num3)) # Maneira mais pratica de saber o menor

if num1 > num2 and num1 > num3:
    print('The more number is {}'.format(num1))
if num2 > num1 and num2 > num3:
    print('The more number is {}'.format(num2))
if num3 > num1 and num3 > num2:
    print('The more number is {}'.format(num3))
if num1 < num2 and num1 < num3:
    print('The smaller number is {}'.format(num1))
if num2 < num1 and num2 < num3:
    print('The smaller number is {}'.format(num2))
if num3 < num1 and num3 < num2:
    print('The smaller number is {}'.format(num3))


