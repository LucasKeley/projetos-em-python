# Leia um programa que leia uma frase pelo teclaro e mostre:
# 1) Quantas veses aparece a letra "A"
# 2) Em que posição ela aparece a primeira vez
# 3) Em que posição ela aparece na última vez

phrase = str(input('Put with a phrase ')).strip()

# 1) 

print('The letters (a) pops up {} times'.format(phrase.lower().count('a')))

# 2)

print('Pops up firts in position {}'.format(phrase.find('a')))

# 3)

print('Pops up last in position {}'.format(phrase.rfind('a')))