# Calculo de Indice de Massa Corporia
# IMC < 18.5 --- Abaixo do Peso
# 18.5 <= IMC <= 25 --- Peso Ideal
# 25 < IMC <= 30 --- Sobre peso
# 30 < IMC <= 40 --- Obesidade
# 40 < IMC --- Obesidade Morbida

print('IMC')
weigth = float(input('Put your weight '))
heigth = float(input('Put your heigth '))
imc = weigth / pow(heigth, 2)

if imc < 18.5:
    print('IMC = {:.2f}'.format(imc))
    print('\033[1;31mUnder weight\033[m')
elif imc >= 18.5 and imc <= 25:
    print('IMC = {:.2f}'.format(imc))
    print('\033[1;32mIdeal weight\033[m') 
elif imc > 25 and imc <= 30:
    print('IMC = {:.2f}'.format(imc))
    print('\033[1;33moverweight\033[m')
elif imc > 30 and imc <= 40:
    print('IMC = {:.2f}'.format(imc))
    print('\033[1;31mObesity\033[m')
elif imc > 40:
    print('IMC = {:.2f}'.format(imc))
    print('\033[1;41mMorbid\033[m')
